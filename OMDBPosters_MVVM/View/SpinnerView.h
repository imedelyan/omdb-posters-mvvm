//
//  SpinnerView.h
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/29/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpinnerView : UIImageView

@end
