//
//  SearchViewController.m
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import "SearchViewController.h"
#import "SpinnerView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ReactiveCocoa/RACEXTScope.h>

@interface SearchViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) SearchViewModel *viewModel;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIButton *searchHistoryButton;
@property (weak, nonatomic) IBOutlet UITableView *searchTableView;
@property (weak, nonatomic) IBOutlet SpinnerView *spinner;

@end

@implementation SearchViewController

- (instancetype)initWithViewModel:(SearchViewModel *)viewModel {
    self = [super init];
    if (self ) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //views alignment starts from UINvagationBar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.searchTableView.delegate = self;
    self.searchTableView.dataSource = self;
    
    [self bindViewModel];
    
    //gesture recognizer to hide kayboard
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self.view action:@selector(endEditing:)];
    [self.view addGestureRecognizer:tap];
    [tap setCancelsTouchesInView:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    //TODO - make it as a Signal form viewWillAppear
    self.title = self.viewModel.title;
    self.searchTextField.text = @"";
    self.viewModel.searchText = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)bindViewModel {
    
    //bindes searchText property with ViewModel's one
    RAC(self.viewModel, searchText) = self.searchTextField.rac_textSignal;
    
    //bindes search button with ViewModel's command executing
    self.searchButton.rac_command = self.viewModel.executeSearch;
    
    //bindes searchHistory button with ViewModel's command executing
    self.searchHistoryButton.rac_command = self.viewModel.executeSearchHistory;
    
    //loading indicator will be shown while executing search
    RAC([UIApplication sharedApplication], networkActivityIndicatorVisible) = self.viewModel.executeSearch.executing;
    RAC(self.spinner, hidden) = [self.viewModel.executeSearch.executing not];
    
    //observes movieSearchResult property of viewModel - reload table data on typing searching text
    @weakify(self)
    [[RACObserve(self, viewModel.movieSearchResult) ignore:nil] subscribeNext : ^(id x) {
        @strongify(self)
        [self.searchTableView reloadData];
    }];
    
    //observs alert message property of ViewModel to show Error massages
    [RACObserve(self, viewModel.alertMessage) subscribeNext : ^(NSString *alert) {
        if (alert) {
            @strongify(self)
            [self showAlertWithTitle:@"Error" andMessage:alert];
        }
    }];
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Table view data source and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.viewModel numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewModel numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.viewModel tableView:tableView cellForRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.viewModel didSelectRowAtIndexPath:indexPath];
}

@end
