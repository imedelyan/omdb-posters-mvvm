//
//  SearchViewController.h
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchViewModel.h"

@interface SearchViewController : UIViewController

- (instancetype)initWithViewModel:(SearchViewModel *)viewModel;

@end
