//
//  SpinnerView.m
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/29/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import "SpinnerView.h"

@implementation SpinnerView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self startAnimating];
}

- (void)startAnimating {
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = [NSNumber numberWithFloat: -2*M_PI];
    animation.toValue = [NSNumber numberWithFloat: 0.0f];
    animation.duration = 2.0f;
    animation.repeatCount = INFINITY;
    animation.removedOnCompletion = NO;
    
    [self.layer addAnimation:animation forKey:@"spin"];
}

@end
