//
//  MovieDetailedViewController.h
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieDetailedViewModel.h"

@interface MovieDetailedViewController : UIViewController

- (instancetype)initWithViewModel:(MovieDetailedViewModel *)viewModel;

@end
