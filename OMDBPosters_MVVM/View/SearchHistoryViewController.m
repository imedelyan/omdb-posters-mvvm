//
//  SearchHistoryViewController.m
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import "SearchHistoryViewController.h"
#import "SearchHistoryTableViewCell.h"

@interface SearchHistoryViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) SearchHistoryViewModel *viewModel;
@property (weak, nonatomic) IBOutlet UITableView *searchHistoryTable;

@end

@implementation SearchHistoryViewController

- (instancetype)initWithViewModel:(SearchHistoryViewModel *)viewModel {
    if (self = [super init]) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchHistoryTable.delegate = self;
    self.searchHistoryTable.dataSource = self;
    [self bindViewModel];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)bindViewModel {

}

#pragma mark - Table view data source and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.viewModel numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewModel numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.viewModel tableView:tableView cellForRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.viewModel didSelectRowAtIndexPath:indexPath];
}

@end
