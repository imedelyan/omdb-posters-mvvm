//
//  MovieDetailedViewController.m
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import "MovieDetailedViewController.h"

@interface MovieDetailedViewController ()

@property (strong, nonatomic) MovieDetailedViewModel *viewModel;

@property (weak, nonatomic) IBOutlet UILabel *movieTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *releaseDateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *posterImage;

@end

@implementation MovieDetailedViewController

- (instancetype)initWithViewModel:(MovieDetailedViewModel *)viewModel {
    if (self = [super init]) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //views alignment starts from UINvagationBar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.movieTitleLabel.text = self.viewModel.movie.title;
    self.movieTitleLabel.adjustsFontSizeToFitWidth = YES;
    self.releaseDateLabel.text = self.viewModel.movie.released;
    [self.posterImage setImage:self.viewModel.moviePosterImage];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
