//
//  ViewModelServicesImplementation.h
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/31/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewModelServices.h"

@interface ViewModelServicesImplementation : NSObject <ViewModelServices>

- (instancetype)initWithNavigationController:(UINavigationController *)navigationController;

@end
