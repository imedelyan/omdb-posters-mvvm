//
//  ViewModelServicesImplementation.m
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/31/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import "ViewModelServicesImplementation.h"
#import "SearchImplementation.h"
#import "SearchHistoryViewController.h"
#import "MovieDetailedViewController.h"

@interface ViewModelServicesImplementation ()

@property (strong, nonatomic) SearchImplementation *searchService;
@property (weak, nonatomic) UINavigationController *navigationController;

@end

@implementation ViewModelServicesImplementation

- (instancetype)initWithNavigationController:(UINavigationController *)navigationController {
    if (self = [super init]) {
        _searchService = [SearchImplementation new];
        _navigationController = navigationController;
    }
    return self;
}

- (id<SearchProtocol>)getSearchService {
    return self.searchService;
}

- (void)pushViewModel:(id)viewModel {
    id viewController;
    
    if ([viewModel isKindOfClass:SearchHistoryViewModel.class]) {
        viewController = [[SearchHistoryViewController alloc] initWithViewModel:viewModel];
    } else if ([viewModel isKindOfClass:MovieDetailedViewModel.class]) {
        viewController = [[MovieDetailedViewController alloc] initWithViewModel:viewModel];
    } else {
        //another viewController
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
