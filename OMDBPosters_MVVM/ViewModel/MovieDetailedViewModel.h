//
//  MovieDetailedViewModel.h
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewModelServices.h"
#import "MovieModel.h"

@interface MovieDetailedViewModel : NSObject

- (instancetype)initWithMovie:(MovieModel *)movie andServices:(id<ViewModelServices>)services;

@property (strong, nonatomic) MovieModel *movie;
@property (strong, nonatomic) UIImage *moviePosterImage;

@end
