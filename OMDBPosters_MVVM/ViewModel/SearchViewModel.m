//
//  SearchViewModel.m
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import "SearchViewModel.h"
#import "MovieModel.h"
#import "MovieDataBase.h"
#import "SearchHistoryViewModel.h"
#import "MovieDetailedViewModel.h"

@interface SearchViewModel () 

@property (nonatomic, weak) id<ViewModelServices> services;
@property (nonatomic, strong) MovieModel *movie;

@end

@implementation SearchViewModel

@synthesize movieSearchResult;
@synthesize movie;

- (instancetype)initWithServices:(id<ViewModelServices>)services {
    self = [super init];
    if (self) {
        _services = services;
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.title = @"Open Movie Database Posters";
    
    //signal from the ViewModel’s searchText property
    RACSignal *searchTextSignal = [RACObserve(self, searchText) distinctUntilChanged];
    [searchTextSignal subscribeNext:^(NSString *text) {
        self.movieSearchResult = [MovieDataBase getMovieArrayforSearchText:text];
    }];
    
    //push View to search history - command
    self.executeSearchHistory =
    [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
                                return [self executeSearchHistorySignal];
                            }];
    
    //command is enabled when search button pressed
    self.executeSearch =
    [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        //checking if searching movie is in local DataBase already
        self.movie = [MovieDataBase getMovieforMovieTitle:self.searchText];
        if (!self.movie) {
            //search for movie on OMDBAPI
            return [self executeSearchSignal];
        } else {
            //load movie data from local DataBase and perform push to DetailedView
            return [RACSignal defer:^{
                [self saveSearchHistory];
                
                MovieDetailedViewModel *detailedViewModel = [[MovieDetailedViewModel alloc] initWithMovie:self.movie andServices:self.services];
                [self.services pushViewModel:detailedViewModel];
                
                return [RACSignal empty];
            }];
        }
    }];
}

- (RACSignal *)executeSearchHistorySignal {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        SearchHistoryViewModel *historyViewModel = [[SearchHistoryViewModel alloc] initWithServices:self.services];
        [self.services pushViewModel:historyViewModel];
        [subscriber sendCompleted];
        return nil;
    }];
}

- (RACSignal *)executeSearchSignal {
    return [[[[self.services getSearchService] searchSignal:self.searchText]
            doNext:^(MovieModel *movie) {
                if (movie) {
                    self.movie = movie;
                    if (![movie.posterURL isEqualToString:@"N/A"]) {
                        
                        // downloading poster image
                        NSURL *url = [NSURL URLWithString:movie.posterURL];
                        NSURLSessionDownloadTask *task = [[NSURLSession sharedSession] downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (!error) {
                                    if (location) {
                                        movie.poster = [NSData dataWithContentsOfURL:location];
                                    }
                                } else {
                                    NSLog(@"Error: %@ ", error.localizedDescription);
                                }
                                //saving to history array and pushing DetailedView for movie
                                [self saveSearchHistory];
                                [MovieDataBase saveMovie:movie];
                                MovieDetailedViewModel *detailedViewModel = [[MovieDetailedViewModel alloc] initWithMovie:movie andServices:self.services];
                                [self.services pushViewModel:detailedViewModel];
                            });
                        }];
                        [task resume];
                        
                    } else {
                        //saving to history array and pushing DetailedView for movie
                        [self saveSearchHistory];
                        [MovieDataBase saveMovie:movie];
                        MovieDetailedViewModel *detailedViewModel = [[MovieDetailedViewModel alloc] initWithMovie:movie andServices:self.services];
                        [self.services pushViewModel:detailedViewModel];
                    }
                } else {
                    self.alertMessage = @"Movie is not found!";
                }
            }]
            doError:^(NSError *error) {
                NSLog(@"Error: %@ ", error.localizedDescription);
                self.alertMessage = @"requesting server API!";
            }];
}

- (void)saveSearchHistory {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (defaults) {
        NSMutableArray *searchHistory = [NSMutableArray new];
        if ([defaults objectForKey:@"searchHistory"]) {
            searchHistory = [NSMutableArray arrayWithArray:[defaults objectForKey:@"searchHistory"]];
        }
        [searchHistory insertObject:self.movie.title atIndex:0];
        [defaults setObject:searchHistory forKey:@"searchHistory"];
        [defaults synchronize];
    }
}

- (UIImage *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    // Create a bitmap context.
    UIGraphicsBeginImageContextWithOptions(newSize, YES, [UIScreen mainScreen].scale);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Table view data source and delegate

- (NSInteger)numberOfSections {
    return 1;
}

- (NSInteger)numberOfRowsInSection:(NSInteger)section {
    return [self.movieSearchResult count];
}

- (SearchHistoryTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchHistoryTableViewCell"];
    
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"SearchHistoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"SearchHistoryTableViewCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"SearchHistoryTableViewCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    MovieModel *movie = [self.movieSearchResult objectAtIndex:indexPath.row];
    
    // setting small poster image
    UIImage *posterImage;
    if (movie.poster) {
        posterImage = [UIImage imageWithData:movie.poster];
    } else {
        posterImage = [UIImage imageNamed:@"noposter"];
    }
    UIImage *smallPosterImage = [self imageWithImage:posterImage scaledToSize:CGSizeMake(27, 40)];
    
    cell.smallPoster.image = smallPosterImage;
    cell.movieTitle.text = movie.title;
    cell.releaseDate.text = movie.released;
    
    return cell;
}

- (void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MovieModel *movie = [self.movieSearchResult objectAtIndex:indexPath.row];
    self.movie = movie;
    [self saveSearchHistory];
    MovieDetailedViewModel *detailedViewModel = [[MovieDetailedViewModel alloc] initWithMovie:movie andServices:self.services];
    [self.services pushViewModel:detailedViewModel];
}

@end
