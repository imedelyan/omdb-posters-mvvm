//
//  SearchViewModel.h
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "ViewModelServices.h"
#import "TableViewProtocol.h"

@interface SearchViewModel : NSObject <TableViewProtocol>

- (instancetype)initWithServices:(id<ViewModelServices>)services;

@property (strong, nonatomic) NSString *searchText;
@property (strong, nonatomic) NSString *title;
@property (nonatomic, strong) NSMutableArray *movieSearchResult;

@property (strong, nonatomic) RACCommand *executeSearch;
@property (strong, nonatomic) RACCommand *executeSearchHistory;

@property (strong, nonatomic) NSString *alertMessage;

@end
