//
//  SearchHistoryViewModel.h
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewModelServices.h"
#import "TableViewProtocol.h"

@interface SearchHistoryViewModel : NSObject <TableViewProtocol>

- (instancetype)initWithServices:(id<ViewModelServices>)services;

@property (nonatomic, strong) NSMutableArray *searchHistoryArray;

@end
