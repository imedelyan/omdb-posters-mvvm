//
//  MovieDetailedViewModel.m
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import "MovieDetailedViewModel.h"

@implementation MovieDetailedViewModel

- (instancetype)initWithMovie:(MovieModel *)movie andServices:(id<ViewModelServices>)services {
    self = [super init];
    if (self) {
        _movie = movie;
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
}

- (UIImage *)moviePosterImage {
    UIImage *image;
    if (self.movie.poster) {
        image = [UIImage imageWithData:self.movie.poster];
    } else {
        image = [UIImage imageNamed:@"noposter"];
    }
    return [self imageWithImage:image convertToSize:CGSizeMake(300, 442)];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

@end
