//
//  SearchHistoryViewModel.m
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import "SearchHistoryViewModel.h"
#import "MovieModel.h"
#import "MovieDataBase.h"
#import "MovieDetailedViewModel.h"

@interface SearchHistoryViewModel () 

@property (nonatomic, weak) id<ViewModelServices> services;

@end

@implementation SearchHistoryViewModel

- (instancetype)initWithServices:(id<ViewModelServices>)services {
    self = [super init];
    if (self) {
        _services = services;
        [self initialize];
    }
    return self;
}

- (void)initialize {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.searchHistoryArray = [NSMutableArray arrayWithArray:[defaults objectForKey:@"searchHistory"]];
}

- (UIImage *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    // Create a bitmap context.
    UIGraphicsBeginImageContextWithOptions(newSize, YES, [UIScreen mainScreen].scale);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Table view data source and delegate

- (NSInteger)numberOfSections {
    return 1;
}

- (NSInteger)numberOfRowsInSection:(NSInteger)section {
    return [self.searchHistoryArray count];
}

- (SearchHistoryTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchHistoryTableViewCell"];
    
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"SearchHistoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"SearchHistoryTableViewCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"SearchHistoryTableViewCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    MovieModel *movie = [MovieDataBase getMovieforMovieTitle:[self.searchHistoryArray objectAtIndex:indexPath.row]];
    
    // setting small poster image
    UIImage *posterImage;
    if (movie.poster) {
        posterImage = [UIImage imageWithData:movie.poster];
    } else {
        posterImage = [UIImage imageNamed:@"noposter"];
    }
    UIImage *smallPosterImage = [self imageWithImage:posterImage scaledToSize:CGSizeMake(27, 40)];
    
    cell.smallPoster.image = smallPosterImage;
    cell.movieTitle.text = movie.title;
    cell.releaseDate.text = movie.released;
    
    return cell;
}

- (void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MovieModel *movie = [MovieDataBase getMovieforMovieTitle:[self.searchHistoryArray objectAtIndex:indexPath.row]];
    MovieDetailedViewModel *detailedViewModel = [[MovieDetailedViewModel alloc] initWithMovie:movie andServices:self.services];
    [self.services pushViewModel:detailedViewModel];
}

@end
