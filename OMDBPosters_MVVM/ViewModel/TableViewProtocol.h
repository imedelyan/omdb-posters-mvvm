//
//  TableViewProtocol.h
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 2/4/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchHistoryTableViewCell.h"

@protocol TableViewProtocol <NSObject>

//Table view data source and delegate for ViewModel
- (NSInteger)numberOfSections;
- (NSInteger)numberOfRowsInSection:(NSInteger)section;
- (SearchHistoryTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

@end
