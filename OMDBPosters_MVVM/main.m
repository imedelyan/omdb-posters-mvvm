//
//  main.m
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/16/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
