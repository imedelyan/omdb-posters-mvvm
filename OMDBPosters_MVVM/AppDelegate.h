//
//  AppDelegate.h
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/16/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

