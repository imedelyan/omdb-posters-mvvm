//
//  SearchProtocol.h
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/31/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <Foundation/Foundation.h>

@protocol SearchProtocol <NSObject>

- (RACSignal *)searchSignal:(NSString *)searchString;

@end
