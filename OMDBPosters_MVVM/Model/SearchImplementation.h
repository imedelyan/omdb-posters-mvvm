//
//  SearchImplementation.h
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/31/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchProtocol.h"

@interface SearchImplementation : NSObject <SearchProtocol>

@end
