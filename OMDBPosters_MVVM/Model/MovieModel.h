//
//  MovieModel.h
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/27/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import <Realm/Realm.h>

@interface MovieModel : RLMObject

@property NSString *title;
@property NSString *released;
@property NSString *posterURL;
@property NSData *poster;

+ (instancetype)modelWithData:(NSDictionary *)data;

@end
