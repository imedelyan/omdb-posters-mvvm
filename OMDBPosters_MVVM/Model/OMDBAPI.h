//
//  OMDBAPI.h
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/27/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

typedef void(^APIManagerBlock)(id result, NSError* error);

@interface OMDBAPI : NSObject

+ (void)getInformationForMovie:(NSString *)movieTitle withCompletion:(APIManagerBlock)completion;

@end
