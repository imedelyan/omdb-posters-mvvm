//
//  MovieDataBase.m
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/28/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import "MovieDataBase.h"

@implementation MovieDataBase

static MovieDataBase *dataBase = nil;

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dataBase = [[self class] new];
    });
    return dataBase;
}

+ (MovieModel *)getMovieforMovieTitle:(NSString *)movieTitle {
    RLMResults *movieArray = [MovieModel allObjects];
    NSLog(@"%@", movieArray);
    for (MovieModel *movie in movieArray) {
        if ([[movie.title lowercaseString] isEqualToString:[movieTitle lowercaseString]]) {
            return movie;
        }
    }
    return nil;
}

+ (NSMutableArray *)getMovieArrayforSearchText:(NSString *)searchText {
    RLMResults *movieArray = [MovieModel allObjects];
    NSMutableArray *array = [NSMutableArray new];
    if (searchText) {
        for (MovieModel *movie in movieArray) {
            if ([[movie.title lowercaseString] containsString:[searchText lowercaseString]]) {
                [array insertObject:movie atIndex:0];
            }
        }
    }
    return array;
}

+ (void)saveMovie:(MovieModel *)movie {
    RLMRealm* realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm addObject:movie];
    [realm commitWriteTransaction];
}

@end
