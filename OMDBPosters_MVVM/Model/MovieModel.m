//
//  MovieModel.m
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/27/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import "MovieModel.h"

@implementation MovieModel

+ (instancetype)modelWithData:(NSDictionary *)data {
    MovieModel *model = [[self class] new];
    [model parseData:data];
    return model;
}

- (void)parseData:(NSDictionary *)data {
    self.title = [data valueForKey:@"Title"];
    self.released = [data valueForKey:@"Released"];
    self.posterURL = [data valueForKey:@"Poster"];
}

@end
