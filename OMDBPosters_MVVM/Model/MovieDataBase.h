//
//  MovieDataBase.h
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/28/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MovieModel.h"

@interface MovieDataBase : NSObject

+ (instancetype)sharedInstance;
+ (MovieModel *)getMovieforMovieTitle:(NSString *)movieTitle;
+ (NSMutableArray *)getMovieArrayforSearchText:(NSString *)searchText;
+ (void)saveMovie:(MovieModel *)movie;

@end
