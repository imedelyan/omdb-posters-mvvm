//
//  SearchImplementation.m
//  OMDBPosters_MVVM
//
//  Created by Igor Medelyan on 1/31/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import "SearchImplementation.h"
#import "MovieModel.h"
#import "OMDBAPI.h"

@implementation SearchImplementation

- (RACSignal *)searchSignal:(NSString *)searchString {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [OMDBAPI getInformationForMovie:searchString withCompletion:^(id result, NSError *error) {
            if (!error && result) {
                if ([[result objectForKey:@"Response"] isEqualToString:@"True"]) {
                    [subscriber sendNext:[MovieModel modelWithData:result]];
                } else {
                    [subscriber sendNext:nil];
                }
            } else {
                [subscriber sendError:error];
            }
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

@end
